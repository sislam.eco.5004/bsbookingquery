﻿using Model.DTO.Hotel;
using Model.DTO.Reply;
using Model.EntityModel;
using Model.Enum;
using Repository.UnitOfWork;
using Utilities.Common;
using Utilities.Exceptions;

namespace Manager
{
    public interface IReplyManager
    {

        int Add(AddReplyInputDto addDto);
        int Update(string guid , AddReplyInputDto updateDto);
        int ChangeStatus(string guid , EnumRStatus status);
    }
    public class ReplyManager : IReplyManager
    {
        private readonly IUnitOfWork _unitOfWork;
        public ReplyManager(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public  int Add(AddReplyInputDto addDto)
        {
            var FkCommentId = _unitOfWork.Comment.FindByCondition(s => s.Guid == addDto.CommentGuid)?.FirstOrDefault()?.Id;
            if (FkCommentId == null)
                throw new BadRequestException("Invalid CommentGuid!");
            var FkStafId = _unitOfWork.Staff.FindByCondition(s => s.Guid == addDto.StaffGuid)?.FirstOrDefault()?.Id;
            if (FkStafId == null)
                throw new BadRequestException("Invalid StaffGuid!");

            ReplyModel addReply = new ReplyModel
            {
                Text = addDto.Text ,
                FkCommentId = FkCommentId ?? 0 ,
                FkStafId = FkStafId ?? 0 ,
                RStatus = EnumRStatus.Active ,
                CreatedBy = 0 ,
                CreatedDate = Common.GetDateLocalDateTime()
            };
            _unitOfWork.Reply.Create(addReply);
            return _unitOfWork.SaveChanges();
        }
        public  int Update(string guid , AddReplyInputDto updateDto)
        {

            var updateReply = _unitOfWork.Reply.FindByCondition(s => s.Guid == guid)?.FirstOrDefault();
            if (updateReply == null)
                throw new BadRequestException("Invalid Reply Guid");
            var FkCommentId = _unitOfWork.Comment.FindByCondition(s => s.Guid == updateDto.CommentGuid)?.FirstOrDefault()?.Id;
            if (FkCommentId == null)
                throw new BadRequestException("Invalid CommentGuid");
            var FkStafId = _unitOfWork.Staff.FindByCondition(s => s.Guid == updateDto.StaffGuid)?.FirstOrDefault()?.Id;
            if (FkStafId == null)
                throw new BadRequestException("Invalid StaffGuid");

            updateReply.Text = updateDto.Text;
            updateReply.FkCommentId = FkCommentId ?? 0;
            updateReply.FkStafId = FkStafId ?? 0;

            _unitOfWork.Reply.Update(updateReply);
            return _unitOfWork.SaveChanges();
        }
        public  int ChangeStatus(string guid , EnumRStatus status)
        {
            CommentModel chReplyStatus = _unitOfWork.Comment
                              .FindByCondition(
                               s => s.Guid == guid)
                              .FirstOrDefault();
            if (chReplyStatus == null)
            {
                throw new BadRequestException("Invalid Guid!");
            }
            chReplyStatus.RStatus = status;

            if (status == EnumRStatus.Deleted)
                _unitOfWork.Comment.Delete(chReplyStatus);
            else
                _unitOfWork.Comment.Update(chReplyStatus);

            return _unitOfWork.SaveChanges();
        }
    }
}
