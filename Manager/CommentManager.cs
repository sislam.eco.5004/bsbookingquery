﻿using Model.DTO.Hotel;
using Model.EntityModel;
using Model.Enum;
using Repository.UnitOfWork;
using Utilities.Common;
using Utilities.Exceptions;

namespace Manager
{
    public interface ICommentManager
    {
        CommentOutputDto GetCommentsById(string guId);
        //Task<List<CommentOutputDto>> GetCommentsByHotelId();
        bool Add(AddCommentInputDto addDto);
        bool Update(string guid , UpdateCommentInputDto updateDto);
        bool ChangeStatus(string guid , EnumRStatus status);
    }
    public class CommentManager : ICommentManager
    {
        private readonly IUnitOfWork _unitOfWork;
        public CommentManager(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public  bool Add(AddCommentInputDto addDto)
        {
            var hotelId = _unitOfWork.Hotel.FindByCondition(s => s.Guid == addDto.HotelGuid)?.FirstOrDefault()?.Id;
            if (hotelId == null)
                throw new BadRequestException("Invalid HotelGuid");
            var customerId = _unitOfWork.Customer.FindByCondition(s => s.Guid == addDto.CustomerGuid)?.FirstOrDefault()?.Id;
            if (customerId == null)
                throw new BadRequestException("Invalid CustomerGuid");

            CommentModel addComments = new CommentModel
            {
                Text = addDto.Comment ,
                FkCustomerId = customerId ?? 0 ,
                FkHotelId = hotelId ?? 0 ,
                RStatus = EnumRStatus.Active ,
                CreatedBy = 0 ,
                CreatedDate = Common.GetDateLocalDateTime()
            };
            _unitOfWork.Comment.Create(addComments);
            var result = _unitOfWork.SaveChanges();
            return true;
        }
        public  bool Update(string guid , UpdateCommentInputDto updateDto)
        {

            var updateomment = _unitOfWork.Comment.FindByCondition(s => s.Guid == updateDto.CommentGuid)?.FirstOrDefault();
            if (updateomment == null)
                throw new BadRequestException("Invalid CommentGuid");
            var hotelId = _unitOfWork.Hotel.FindByCondition(s => s.Guid == updateDto.HotelGuid)?.FirstOrDefault()?.Id;
            if (hotelId == null)
                throw new BadRequestException("Invalid HotelGuid");
            var customerId = _unitOfWork.Customer.FindByCondition(s => s.Guid == updateDto.CustomerGuid)?.FirstOrDefault()?.Id;
            if (customerId == null)
                throw new BadRequestException("Invalid CustomerGuid");

            updateomment.Text = updateDto.Comment;
            updateomment.FkCustomerId = customerId ?? 0;
            updateomment.FkCustomerId = hotelId ?? 0;

            _unitOfWork.Comment.Update(updateomment);
            _unitOfWork.SaveChanges();
            return true;
        }
        public  bool ChangeStatus(string guid , EnumRStatus status)
        {
            CommentModel? chStatsuText = _unitOfWork.Comment
                              .FindByCondition(
                               s => s.Guid == guid)
                              .FirstOrDefault();
            if (chStatsuText == null)
            {
                throw new BadRequestException("Invalid Guid!");
            }
            chStatsuText.RStatus = status;

            if (status == EnumRStatus.Deleted)
                _unitOfWork.Comment.Delete(chStatsuText);
            else
                _unitOfWork.Comment.Update(chStatsuText);

            _unitOfWork.SaveChanges();
            return true;
        }

        public  CommentOutputDto GetCommentsById(string guId)
        {
            CommentOutputDto Comment =  (from comment in _unitOfWork.Comment
                                        .FindByCondition(s => s.Guid == guId)
                                         join hotelList in _unitOfWork.Hotel.FindAll()
                                         on comment.FkHotelId equals hotelList.Id
                                         into hotels
                                         from hotel in hotels.DefaultIfEmpty()
                                         join customerList in _unitOfWork.Customer.FindAll()
                                         on comment.FkCustomerId equals customerList.Id
                                         into customers
                                         from customer in customers.DefaultIfEmpty()
                                         join replyList in _unitOfWork.Hotel.Db.Replies
                                         on comment.Id equals replyList.FkCommentId
                                         into replies
                                         from reply in replies.DefaultIfEmpty()
                                         join staffList in _unitOfWork.Hotel.Db.Staffs
                                       on reply.FkStafId equals staffList.Id
                                       into staffs
                                         from staff in staffs.DefaultIfEmpty()
                                         group new { comment , hotel , customer , reply , staff } by new {
                                             CommentGuid = comment.Guid ,
                                             Comment = comment.Text ,
                                             CustomerGuid = customer.Guid ,
                                             HotelGuid = hotel.Guid ,
                                             CustomerName = customer.Name
                                         }
                                          into CommentDetails
                                         select new CommentOutputDto
                                         {
                                             CommentGuid = CommentDetails.Key.CommentGuid ,
                                             Comment = CommentDetails.Key.Comment ,
                                             CustomerGuid = CommentDetails.Key.CustomerGuid ,
                                             HotelGuid = CommentDetails.Key.HotelGuid ,
                                             CustomerName = CommentDetails.Key.CustomerName ,
                                             Replies = CommentDetails.ToList()
                                             .Select(s => new ReplyDto 
                                             { ReComment = s.reply.Text , 
                                                 ReplyGuid = s.reply.Guid , 
                                                 StaffGuid = s.staff.Guid , 
                                                 StaffName = s.staff.Name 
                                             }).ToList()
                                         }).FirstOrDefault();
            return Comment;
        }
    }
}
