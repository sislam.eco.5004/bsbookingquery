﻿using Microsoft.EntityFrameworkCore;
using Model.DTO.Hotel;
using Model.EntityModel;
using Model.Enum;
using Repository.UnitOfWork;
using Utilities.Common;
using Utilities.Exceptions;

namespace Manager
{
    public interface IHotelManager
    {
        Task<HotelOutputDto> GetHotel(string guId);
        Task<List<HotelOutputDto>> GetHotelList(HotelSearchInputDto hotelSearchInputDto);
        Task<int> Add(HotelInputDto addDto);
        Task<int> Update(string guid , HotelInputDto updateDto);
        Task<int> ChangeStatus(string guid , EnumRStatus status);
    }
    public class HotelManager : IHotelManager
    {
        IUnitOfWork _unitOfWork;
        public HotelManager(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public async Task<HotelOutputDto> GetHotel(string guId)
        {
            var hotel = await _unitOfWork.Hotel
                                               .FindByCondition(s => s.Guid == guId)
                                               .AsNoTracking().Select(s => new HotelOutputDto
                                               {
                                                   Guid = s.Guid ,
                                                   Location = s.Location ,
                                                   Name = s.Name ,
                                                   Rating = s.Rating
                                               })
                                               .FirstOrDefaultAsync();

            return hotel;
        }

        public async Task<List<HotelOutputDto>> GetHotelList(HotelSearchInputDto hotelSearchInputDto)
        {
            if (string.IsNullOrEmpty(hotelSearchInputDto.SearchText))
                hotelSearchInputDto.SearchText = string.Empty;
            else
                hotelSearchInputDto.SearchText = hotelSearchInputDto.SearchText.Trim().ToLower();

            if (string.IsNullOrEmpty(hotelSearchInputDto.Guid))
                hotelSearchInputDto.Guid = string.Empty;
            if (hotelSearchInputDto.RatingRangeStart == null)
                hotelSearchInputDto.RatingRangeStart = 0;
            if (hotelSearchInputDto.RatingRangeEnd == null)
                hotelSearchInputDto.RatingRangeEnd = 0;

            var hotels = await _unitOfWork.Hotel
                                              .FindByCondition(s =>
                                              s.RStatus == EnumRStatus.Active)
                                              .Where(s =>
                                              (hotelSearchInputDto.SearchText == ""
                                              || (s.Location.Trim().ToLower().Contains(hotelSearchInputDto.SearchText) || s.Name.Trim().ToLower().Contains(hotelSearchInputDto.SearchText)))
                                              &&
                                              (hotelSearchInputDto.Guid == "" || s.Guid == hotelSearchInputDto.Guid)
                                              &&
                                                ((hotelSearchInputDto.RatingRangeStart == 0 && hotelSearchInputDto.RatingRangeEnd == 0)
                                              || (s.Rating >= hotelSearchInputDto.RatingRangeStart && s.Rating <= hotelSearchInputDto.RatingRangeEnd)))
                                              .AsNoTracking().Select(s => new HotelOutputDto
                                              {
                                                  Guid = s.Guid ,
                                                  Location = s.Location ,
                                                  Name = s.Name ,
                                                  Rating = s.Rating
                                              }).ToListAsync(); ;

            return hotels;
        }
        public async Task<int> Add(HotelInputDto addDto)
        {
            var LastHotelId = _unitOfWork.Hotel.FindAll().Max(s => s.Id);
            HotelModel addModel = new HotelModel
            {
                Name = addDto.Name ,
                Location = addDto.Location ,
                Guid = Common.GetUniqueId() ,
                RStatus = EnumRStatus.Active ,
                CreatedBy = 0 ,
                HotelCode = $"S-{LastHotelId + 1}" , // Generate by Requirment
                CreatedDate = Common.GetDateLocalDateTime()
            };
            _unitOfWork.Hotel.Create(addModel);
           return _unitOfWork.SaveChanges();
        }
        public async Task<int> Update(string guid , HotelInputDto updateDto)
        {
            HotelModel? editHotel = _unitOfWork.Hotel
                                .FindByCondition(
                                 s => s.Guid == guid
                                    && s.RStatus == EnumRStatus.Active)
                                .FirstOrDefault();
            if (editHotel == null)
            {
                throw new BadRequestException("Invalid Guid!");
            }
            editHotel.Location = updateDto.Location;
            editHotel.Name = updateDto.Name;
            _unitOfWork.Hotel.Update(editHotel);
            return _unitOfWork.SaveChanges();
        }
        public async Task<int> ChangeStatus(string guid , EnumRStatus status)
        {
            HotelModel? chStatsuHotel = _unitOfWork.Hotel
                              .FindByCondition(
                               s => s.Guid == guid)
                              .FirstOrDefault();
            if (chStatsuHotel == null)
            {
                throw new BadRequestException("Invalid Guid!");
            }
            chStatsuHotel.RStatus = status;

            if (status == EnumRStatus.Deleted)
                _unitOfWork.Hotel.Delete(chStatsuHotel);
            else
                _unitOfWork.Hotel.Update(chStatsuHotel);

            return _unitOfWork.SaveChanges();
        }

    }
}
