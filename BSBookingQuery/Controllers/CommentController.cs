﻿using Manager;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Model.DTO;
using Model.DTO.Hotel;
using Model.DTO.Response;
using Model.Enum;
using Swashbuckle.AspNetCore.Annotations;

namespace BSBookingQuery.Controllers
{
    [Route("api/comment")]
    [ApiController]
    public class CommentController : ControllerBase
    {
        ICommentManager _commentManager;
        public CommentController(ICommentManager commentManager)
        {
            _commentManager = commentManager;
        }
        // single row

        [HttpGet("{guid}")]
        public ObjectResponse<CommentOutputDto> GetCommentsById(string guid)
        {
            var Result = _commentManager.GetCommentsById(guid);
            return new ObjectResponse<CommentOutputDto>()
            {
                Data = Result ,
                Message = Result == null ? "No Data Found" : "Data Found"
            };
        }

        //[HttpGet("hotelGuid")]
        //public async Task<ListResponse<HotelOutDto>> GetCommentByHotelId(string hotelGuid)
        //{
        //    var Result = await _commentManager.GetCommentByHotelId(hotelGuid);
        //    return new ListResponse<HotelOutDto>()
        //    {
        //        Data = Result ,
        //        Message = Result.Count() > 0 ? "No Data Found" : "Data Found"
        //    };
        //}

        [HttpPost]
        public ObjectResponse<bool> Add(AddCommentInputDto addDto)
        {
            var Result = _commentManager.Add(addDto);
            return new ObjectResponse<bool>()
            {
                Data = Result ,
                Message = "Save Successfully"
            };
        }

        [HttpPut("guid")]
        public ObjectResponse<bool> Update(string guid , UpdateCommentInputDto editDto)
        {
            var Result = _commentManager.Update(guid , editDto);
            return new ObjectResponse<bool>()
            {
                Data = Result ,
                Message = "Save Successfully"
            };
        }

        [HttpDelete("guid")]
        public ObjectResponse<bool> Delete(string guid , EnumRStatus status)
        {
            var Result = _commentManager.ChangeStatus(guid , status);
            return new ObjectResponse<bool>()
            {
                Data = Result ,
                Message = "Save Successfully"
            };
        }

    }
}
