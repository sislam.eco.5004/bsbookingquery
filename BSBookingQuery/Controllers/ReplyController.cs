﻿using Manager;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Model.DTO;
using Model.DTO.Hotel;
using Model.DTO.Reply;
using Model.DTO.Response;
using Model.Enum;
using Swashbuckle.AspNetCore.Annotations;

namespace BSBookingQuery.Controllers
{
    [Route("api/reply")]
    [ApiController]
    public class ReplyController : ControllerBase
    {
        IReplyManager _replyManager;
        public ReplyController(IReplyManager replyManager)
        {
            _replyManager = replyManager;
        }
        [HttpPost]
        public ObjectResponse<int> Add(AddReplyInputDto addDto)
        {
            var ReplyId = _replyManager.Add(addDto);
            return new ObjectResponse<int>()
            {
                Data = ReplyId ,
                Message = "Save Successfully"
            };
        }

        [HttpPut("guid")]
        public ObjectResponse<int> Update(string guid , AddReplyInputDto editDto)
        {
            var ReplyId = _replyManager.Update(guid , editDto);
            return new ObjectResponse<int>()
            {
                Data = ReplyId ,
                Message = "Save Successfully"
            };
        }

        [HttpDelete("guid")]
        public ObjectResponse<int> Delete(string guid , EnumRStatus status)
        {
            var ReplyId = _replyManager.ChangeStatus(guid , status);
            return new ObjectResponse<int>()
            {
                Data = ReplyId ,
                Message = "Save Successfully"
            };
        }

    }
}
