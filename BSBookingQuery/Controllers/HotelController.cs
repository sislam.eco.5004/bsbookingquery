﻿using Manager;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Model.DTO;
using Model.DTO.Hotel;
using Model.DTO.Response;
using Model.Enum;
using Swashbuckle.AspNetCore.Annotations;

namespace BSBookingQuery.Controllers
{
    [Route("api/hotel")]
    [ApiController]
    public class HotelController : ControllerBase
    {
        IHotelManager _hotelManager;
        public HotelController(IHotelManager hotelManager)
        {
            _hotelManager = hotelManager;
        }

        [HttpGet("{guid}")]
        public async Task<ObjectResponse<HotelOutputDto>> GetById(string guid)
        {
            var Result = await _hotelManager.GetHotel(guid);
            return new ObjectResponse<HotelOutputDto>()
            {
                Data = Result ,
                Message = Result == null ? "No Data Found" : "Data Found" // Shoude Dynamic msg return
            };
        }

        [HttpGet]
        public async Task<ListResponse<HotelOutputDto>> GetHotels([FromQuery] HotelSearchInputDto hotelSearchInputDto)
        {
            var Result = await _hotelManager.GetHotelList(hotelSearchInputDto);
            return new ListResponse<HotelOutputDto>()
            {
                Data = Result ,
                Message = Result.Count() > 0 ? "No Data Found" : "Data Found"  
            };
        }

        [HttpPost]
        public async Task<ObjectResponse<int>> Add(HotelInputDto addDto)
        {
            var HotelId = await _hotelManager.Add(addDto);
            return new ObjectResponse<int>()
            {
                Data = HotelId ,
                Message = "Save Successfully"
            };
        }

        [HttpPut("guid")]
        public async Task<ObjectResponse<int>> Update(string guid , HotelInputDto editDto)
        {
            var HotelId = await _hotelManager.Update(guid , editDto);
            return new ObjectResponse<int>()
            {
                Data = HotelId ,
                Message = "Save Successfully"
            };
        }
        [HttpPatch("guid")]
        public async Task<ObjectResponse<int>> ChangeStatus(string guid, EnumRStatus status)
        {
            var HotelId = await _hotelManager.ChangeStatus(guid, status);
            return new ObjectResponse<int>()
            {
                Data = HotelId ,
                Message = "Save Successfully"
            };
        }

        [HttpDelete("guid")]
        public async Task<ObjectResponse<int>> Delete(string guid , EnumRStatus status)
        {
            var HotelId = await _hotelManager.ChangeStatus(guid , status);
            return new ObjectResponse<int>()
            {
                Data = HotelId ,
                Message = "Save Successfully"
            };
        }


    }
}
