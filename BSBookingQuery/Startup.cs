using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using System.Data;
using System.Text.Json;
using Repository;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Swashbuckle.AspNetCore.SwaggerUI;
using Microsoft.AspNetCore.ResponseCompression;
using Serilog;
using Serilog.Events;
using utilities.Extensions;
using Microsoft.Data.SqlClient;
using BSBookingQuery.Configuration;
using utilities.Exceptions;
using Abp.Extensions;
using Repository.Dapper;
using Repository.Repo;
using Manager;
using Repository.UnitOfWork;

namespace BSBookingQuery
{
    public class Startup
    {
        private const string _defaultCorsPolicyName = "localhost";
        private readonly IConfigurationRoot _appConfiguration;


        public Startup(IWebHostEnvironment env , IConfiguration configuration)
        {
            _appConfiguration = env.GetAppConfiguration();
            Configuration = configuration;
            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(configuration)
                .Filter.ByIncludingOnly((le) => le.Level == LogEventLevel.Error)
                .CreateLogger();
        }
        public IConfiguration Configuration { get; }
        public void ConfigureServices(IServiceCollection services)
        {
            var connectionstring = _appConfiguration.GetConnectionString("conn-sql");
            services.AddDbContext<DbEntity>(options => options.UseSqlServer(connectionstring));
            services.AddTransient<IDbConnection>(db => new SqlConnection(connectionstring));
            services.AddDbContext<DbEntity>(ServiceLifetime.Transient);
            services.AddTransient<Repository.UnitOfWork.IUnitOfWork , UnitOfWork>();
            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.SuppressModelStateInvalidFilter = true;
            });
            services.AddResponseCompression(options =>
            {
                options.Providers.Add<BrotliCompressionProvider>();
            });
            services.AddMvc(options =>
            {
                options.Filters.Add<DtoValidationFilter>(); // Dto Validation

                options.Filters.Add(new ResponseCacheAttribute { NoStore = true , Location = ResponseCacheLocation.None });
            });
            // Token validateion 
            services.AddDbContext<DbEntity>(ServiceLifetime.Transient);
            ConfigJwtToken(services);
            ConfigSignarR(services);
            ConfigSwagger(services);
            services.AddScoped<IHotelRepository, HotelRepository>();
            services.AddScoped<IHotelManager , HotelManager>();

            services.AddScoped<ICommentManager , CommentManager>();
            services.AddScoped<ICommentRepository , CommentRepository>();

            services.AddScoped<ICustomerRepository , CustomerRepository>();
            services.AddCors();
            List<string> allowedUrls = new List<string>();
            allowedUrls.Add("http://localhost:4200");
            allowedUrls.Add("http://localhost:4201");
            allowedUrls.Add("http://localhost:4202");

            services.AddCors(
                options => options.AddPolicy(
                    _defaultCorsPolicyName ,
                    builder => builder
                        .WithOrigins(
                            allowedUrls.Select(o => o.RemovePostFix("/"))
                                .ToArray())
                        .AllowAnyHeader()
                        .AllowAnyMethod()
                        .AllowCredentials()
                )
            );
            services.AddDistributedMemoryCache();
            services.AddControllers().AddNewtonsoftJson(options => options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);
            ConfigDIManagerProject(services); 
            services.TryAddSingleton<IHttpContextAccessor , HttpContextAccessor>();
        }

        private static void ConfigDIManagerProject(IServiceCollection services)
        {
          

        }
        private static void ConfigSwagger(IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1" , new OpenApiInfo { Title = "s3" , Version = "v1" , Description = "API Version 1.0" });


                c.EnableAnnotations();

                c.AddSecurityDefinition("Bearer" , new OpenApiSecurityScheme
                {
                    Description = "JWT scheme" ,
                    Name = "Authorization" ,
                    In = ParameterLocation.Header ,
                    Scheme = "Bearer"
                });

                c.AddSecurityRequirement(new OpenApiSecurityRequirement()
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            },
                            Scheme = "oauth2",
                            Name = "Bearer",
                            In = ParameterLocation.Header,

                        },
                        new List<string>()
                    }
                });
            });
        }
        private static void ConfigSignarR(IServiceCollection services)
        {
            services.AddSignalR()
                           .AddJsonProtocol(options =>
                           {
                               options.PayloadSerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
                           }
                           );
            services.AddControllers().AddNewtonsoftJson(options =>
                options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            );
        }
        private void ConfigJwtToken(IServiceCollection services)
        {
            //services.AddAuthentication(x =>
            //{
            //    //x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            //    //x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            //}).AddJwtBearer(options =>
            //{
            //    options.RequireHttpsMetadata = false;
            //    options.SaveToken = true;
            //    options.TokenValidationParameters = new TokenValidationParameters
            //    {
            //        ValidateIssuer = false ,
            //        ValidateAudience = false ,
            //        ValidateLifetime = true ,
            //        ValidateIssuerSigningKey = true ,
            //        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(AppSettings.Jwt.SecretKey)) ,
            //        ClockSkew = TimeSpan.Zero
            //    };
            //    services.AddCors();
            //});
        }
        public void Configure(
            IApplicationBuilder app ,
            IWebHostEnvironment env
            )
        {


            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseCors(_defaultCorsPolicyName);
            }

            if (env.IsStaging())
            {
                app.UseCors(_defaultCorsPolicyName);
            }

            if (env.IsProduction())
            {
                app.UseCors(_defaultCorsPolicyName);

            }

            app.UseMiddleware(typeof(ErrorHandlingMiddleware));
            app.UseCookiePolicy(new CookiePolicyOptions()
            {
                MinimumSameSitePolicy = SameSiteMode.Strict
            });

            app.UseStaticFiles(new StaticFileOptions()
            {
                OnPrepareResponse = ctx =>
                {
                    ctx.Context.Response.Headers.Append("Access-Control-Allow-Origin" , "*");
                    ctx.Context.Response.Headers.Append("Access-Control-Allow-Headers" ,
                      "Origin, X-Requested-With, Content-Type, Accept");
                } ,

            });

            app.UseRouting();

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });


            app.UseResponseCompression();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json" , "BSBooking");
                c.InjectStylesheet("/swagger-ui/custom.css");
                c.InjectJavascript("/swagger-ui/custom.js" , "text/javascript");
                c.DocumentTitle = "BSBooking";
                c.DefaultModelsExpandDepth(-1);
                c.DocExpansion(DocExpansion.None);
            });



        }

    }

}