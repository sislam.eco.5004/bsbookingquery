﻿namespace Model.DTO.Response
{
    public class ListResponse<T>
    {
        public string Message { get; set; }
        public List<T> Data { get; set; }
    }
}
