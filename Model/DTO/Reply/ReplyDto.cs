﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.DTO.Reply
{
    public class AddReplyInputDto
    {
        public string Text { get; set; }
        public string CommentGuid { get; set; }
        public string StaffGuid { get; set; }
    }
}
