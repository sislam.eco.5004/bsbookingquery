﻿using Newtonsoft.Json;

namespace Model.DTO
{
    public class ErrorResponse
    {
        [JsonProperty("statusCode")]
        public int StatusCode { get; set; }
        [JsonProperty("timestamp")]
        public DateTime TimeStamp { get; set; } = DateTime.Now;
        [JsonProperty("message")]
        public string Message { get; set; }
        [JsonProperty("description")]
        public object Description { get; set; }
        [JsonProperty("errors")]
        public object Errors { get; set; }
    }
    public class ErrorData
    {
        public string Message { get; set; }
        public string Description { get; set; }
        public List<string> Errors { get; set; }
        public ErrorData()
        {
        }
        public ErrorData(string message, string description)
        {
            Message = message;
            Description = description;
        }
        public ErrorData(string message, string description, List<string> errors)
        {
            Message = message;
            Description = description;
            Errors = errors;
        }
    }
}
