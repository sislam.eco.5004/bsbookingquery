﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.DTO.Hotel
{
    public class AddCommentInputDto: CommonInput
    {
    }
    public class UpdateCommentInputDto: CommonInput
    {
        [Required]
        public string CommentGuid { get; set; }
    }

    public class CommonInput
    {
        [Required]
        public string Comment { get; set; }
        [Required]
        public string HotelGuid { get; set; }
        [Required]
        public string CustomerGuid { get; set; }
        [Required]
        public string CommentGuid { get; set; }
    }


    public class CommentOutputDto
    {
        public string CommentGuid { get; set; }
        public string HotelGuid { get; set; }
        [Required]
        public string Comment { get; set; }
        public string CustomerGuid { get; set; }
        public string CustomerName { get; set; }
        public List<ReplyDto> Replies { get; set; }
    }
    public class ReplyDto
    {
        public string ReplyGuid { get; set; }
        public string StaffGuid { get; set; }
        public string StaffName { get; set; }
        public string ReComment { get; set; }
    }

    
}
