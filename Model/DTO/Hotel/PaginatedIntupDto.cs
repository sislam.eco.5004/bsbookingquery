﻿using Model.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.DTO.Hotel
{
    public class HotelSearchInputDto
    {
        public string SearchText { get; set; }
        public string Guid { get; set; } // Hotel Id
        public int? RatingRangeStart { get; set; }
        public int? RatingRangeEnd { get; set; }
    }
}
