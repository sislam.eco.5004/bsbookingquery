﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.DTO.Hotel
{
    public class HotelOutputDto : HotelInputDto
    {
        public string Guid { get; set; }
    }
    public class HotelInputDto
    {
        public string Name { get; set; }
        public string Location { get; set; }
        public int Rating { get; set; }
    }
}
