﻿namespace Model.Enum
{
    public enum EnumRStatus
    {
        Active = 1,
        Inactive = 2,
        Deleted = 3
    }
}
