﻿using Repository.EntityModel;

namespace Model.EntityModel
{
    public class StaffModel : BaseEntity<int>
    {
        public string Guid { get; set; }
        public string StafCode { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public virtual ICollection<ReplyModel> Replies { get; set; } = new HashSet<ReplyModel>();
    }
}
