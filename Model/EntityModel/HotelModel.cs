﻿using Repository.EntityModel;
using System.ComponentModel.DataAnnotations;


namespace Model.EntityModel
{
    public class HotelModel : BaseEntity<int>
    {
        public string Guid { get; set; }
        public string HotelCode { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }
        public int Rating { get; set; }
        public virtual ICollection<CommentModel> Comments { get; set; } = new HashSet<CommentModel>();
    }



}
