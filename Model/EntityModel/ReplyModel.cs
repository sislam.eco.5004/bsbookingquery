﻿using Repository.EntityModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.EntityModel
{
    public class ReplyModel : BaseEntity<int>
    {
        public int FkCommentId { get; set; }
        [ForeignKey("FkCommentId")]
        public virtual CommentModel Comment { get; set; }
        public int FkStafId { get; set; }
        [ForeignKey("FkStafId")]
        public virtual StaffModel Staff { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public string Guid { get; set; }
    }
}
