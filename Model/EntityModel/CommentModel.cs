﻿using Repository.EntityModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.EntityModel
{
    public class CommentModel : BaseEntity<int>
    {
        public int FkHotelId { get; set; }
        [ForeignKey("FkHotelId")]
        public virtual HotelModel Hotel { get; set; }
        public int FkCustomerId { get; set; }
        [ForeignKey("FkCustomerId")]
        public virtual CustomerModel Customer { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public string Guid { get; set; }
        public virtual ICollection<ReplyModel> Replies { get; set; } = new HashSet<ReplyModel>();
    }
}
