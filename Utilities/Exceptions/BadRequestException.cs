﻿using Model.DTO;
using System;
using System.Net;

namespace Utilities.Exceptions;

public class BadRequestException : Exception
{
    public ErrorData ErrorDetail { get; }
    public string Message { get; }

    public BadRequestException(string message) {
        Message = message;
    }
    public BadRequestException(ErrorData errors)
    {
        ErrorDetail = errors;
    }
}
