﻿using System.Data;
using System.Net;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Model.DTO;
using Newtonsoft.Json;
using Utilities.Exceptions;

namespace utilities.Exceptions
{
    public class ErrorHandlingMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<ErrorHandlingMiddleware> _logger;

        public ErrorHandlingMiddleware(RequestDelegate next , ILogger<ErrorHandlingMiddleware> logger)
        {
            this._next = next;
            _logger = logger;
        }
        public async Task Invoke(HttpContext context , IDbConnection db)
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context , ex);
            }
        }

        private ErrorResponse GetErrorResponse(Exception ex , int statusCode , ErrorData ed)
        {
            ErrorResponse errRsponse = new ErrorResponse();
            _logger.LogError(ex , "REST ERROR");
            errRsponse.StatusCode = statusCode;
            errRsponse.Message = ed.Message;
            errRsponse.Description = ed.Description;
            errRsponse.Errors = ed.Errors;
            return errRsponse;
        }

        private async Task HandleExceptionAsync(HttpContext context , Exception exception)
        {
            ErrorResponse errRsponse = new ErrorResponse();

            switch (exception)
            {
                case BadRequestException re:
                    errRsponse = GetErrorResponse(exception , (int)HttpStatusCode.BadRequest , re.ErrorDetail);
                    break;
                case Exception e:
                    errRsponse = GetErrorResponse(exception , (int)HttpStatusCode.InternalServerError , new ErrorData("Error" , "Something went wrong!"));
                    break;
            }
            context.Response.ContentType = "application/json";
            if (errRsponse.StatusCode > 0)
            {
                context.Response.StatusCode = errRsponse.StatusCode;
            }

            if (errRsponse != null)
            {

                var result = JsonConvert.SerializeObject(errRsponse);
                await context.Response.WriteAsync(result);
            }
        }

    }


}