﻿
namespace Utilities.Common
{
    public class Common
    {
        public static DateTime GetDateLocalDateTime()
        {
            DateTime dateTime = DateTime.Now;
            try
            {
                DateTime date_time = dateTime;
                DateTime temp_value = DateTime.SpecifyKind(
                    date_time,
                    DateTimeKind.Utc);
                return temp_value.ToLocalTime();
            }
            catch (Exception e)
            {
                throw;
            }

        }
        public static string GetUniqueId()
        {
            return (Guid.NewGuid().ToString() + Guid.NewGuid().ToString()).Replace("-", "");
        }
    }
}
