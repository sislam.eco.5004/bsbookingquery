﻿using Microsoft.AspNetCore.Mvc.Filters;
using Model.DTO;
using Utilities.Exceptions;

namespace utilities.Extensions
{
    public class DtoValidationFilter : IAsyncActionFilter
    {
        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            
            var noValidateProperty = context.ModelState
                .Where(x => x.Value.Errors.Select(y => y.ErrorMessage).FirstOrDefault() == "backendValidation")
                .Select(x => x.Key).ToList();
            noValidateProperty.ForEach(x => { context.ModelState.Remove(x); });

            if (!context.ModelState.IsValid)
            {
                var errorsInModelState = context.ModelState
                    .Where(x => x.Value.Errors.Count > 0)
                    .ToDictionary(kvp => kvp.Key,
                        kvp => kvp.Value.Errors.Select(x => x.ErrorMessage)).ToArray();

                var validationResponse = new List<string>();

                foreach (var error in errorsInModelState)
                {
                    foreach (var subError in error.Value)
                    {
                        var fieldName = error.Key.StartsWith("$.") ? error.Key.Remove(0, 2) : error.Key;
                        validationResponse.Add(error.Key.StartsWith("$.") ? "Conversion failed" : subError);
                    }
                }

                ErrorData errorRequest = new ErrorData();
                errorRequest.Errors = validationResponse;
                errorRequest.Message = "Invalid!";
                errorRequest.Description = "Validation failed.";

                throw new BadRequestException(errorRequest);
            }

            await next();

        }
    }
}