﻿using Repository.Configuration;
using Microsoft.EntityFrameworkCore;
using Model.EntityModel;
using Model.Enum;
using Utilities.Common;

namespace Repository
{
    public class DbEntity : DbContext
    {
        public DbEntity(DbContextOptions<DbEntity> options) : base(options)
        {
        }
        public DbSet<HotelModel> Hotels { get; set; }
        public DbSet<CommentModel> Comments { get; set; }
        public DbSet<ReplyModel> Replies { get; set; }
        public DbSet<CustomerModel> Customers { get; set; }
        public DbSet<StaffModel> Staffs { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }
            modelBuilder.ApplyBaseEntityConfiguration();

            modelBuilder.Entity<CustomerModel>().HasData(
                                                        new CustomerModel
                                                        {
                                                            Id = 1 ,
                                                            Name = "Haris" ,
                                                            CustomerCode = "C-1" ,
                                                            Guid = Common.GetUniqueId() ,
                                                            Email = "khalid.eco.5@gmail.com" ,
                                                            Phone = "123654" ,
                                                            RStatus = EnumRStatus.Active ,
                                                            CreatedBy = 1 ,
                                                            CreatedDate = DateTime.Now
                                                        } ,
                                                        new CustomerModel
                                                        {
                                                            Id = 2 ,
                                                            Name = "Ubaydah" ,
                                                            CustomerCode = "C-2" ,
                                                            Guid = Common.GetUniqueId() ,
                                                            RStatus = EnumRStatus.Active ,
                                                            Email = "khalid.eco.5@gmail.com" ,
                                                            Phone = "1236545" ,
                                                            CreatedBy = 1 ,
                                                            CreatedDate = DateTime.Now
                                                        } ,
                                                        new CustomerModel
                                                        {
                                                            Id = 3 ,
                                                            Name = "Saiful" ,
                                                            CustomerCode = "C-3" ,
                                                            Guid = Common.GetUniqueId() ,
                                                            Email = "khalid.eco.5@gmail.com" ,
                                                            Phone = "1236546" ,
                                                            RStatus = EnumRStatus.Active ,
                                                            CreatedBy = 1 ,
                                                            CreatedDate = DateTime.Now
                                                        });

            modelBuilder.Entity<StaffModel>().HasData(
                                                        new StaffModel
                                                        {
                                                            Id = 1 ,
                                                            Name = "Haris" ,
                                                            StafCode = "S-1" ,
                                                            Guid = Common.GetUniqueId() ,
                                                            Phone = "***256" ,
                                                            Email = "1@gmail.com" ,
                                                            RStatus = EnumRStatus.Active ,
                                                            CreatedBy = 1 ,
                                                            CreatedDate = DateTime.Now
                                                        } ,
                                                        new StaffModel
                                                        {
                                                            Id = 2 ,
                                                            Name = "Ubaydah" ,
                                                            StafCode = "S-2" ,
                                                            Guid = Common.GetUniqueId() ,
                                                            Phone = "***257" ,
                                                            Email = "2@gmail.com" ,
                                                            RStatus = EnumRStatus.Active ,
                                                            CreatedBy = 1 ,
                                                            CreatedDate = DateTime.Now
                                                        } ,
                                                        new StaffModel
                                                        {
                                                            Id = 3 ,
                                                            Name = "Saiful" ,
                                                            StafCode = "S-3" ,
                                                            Guid = Common.GetUniqueId() ,
                                                            Phone = "***258" ,
                                                            Email = "3@gmail.com" ,
                                                            RStatus = EnumRStatus.Active ,
                                                            CreatedBy = 1 ,
                                                            CreatedDate = DateTime.Now
                                                        });

            modelBuilder.Entity<HotelModel>().HasData(
                                                       new HotelModel
                                                       {
                                                           Id = 1 ,
                                                           Name = " Radisson Blu Dhaka Water Garden" ,
                                                           HotelCode = "H-1" ,
                                                           Guid = Common.GetUniqueId() ,
                                                           Location = "Airport Road, Dhaka Cantonment, Dhaka City 1206 Bangladesh" ,
                                                            Rating = 3,
                                                           RStatus = EnumRStatus.Active ,
                                                           CreatedBy = 1 ,
                                                           CreatedDate = DateTime.Now
                                                       } ,
                                                       new HotelModel
                                                       {
                                                           Id = 2 ,
                                                           Name = "Pan Pacific Sonargaon Dhaka" ,
                                                           HotelCode = "H-2" ,
                                                           Guid = Common.GetUniqueId() ,
                                                           Location = "107 Kazi Nazrul Islam Avenue GPO Box 3595, Dhaka City 1215 Bangladesh" ,
                                                            Rating = 5,
                                                           RStatus = EnumRStatus.Active ,
                                                           CreatedBy = 1 ,
                                                           CreatedDate = DateTime.Now
                                                       } ,
                                                       new HotelModel
                                                       {
                                                           Id = 3 ,
                                                           Name = "InterContinental Dhaka, an IHG Hotel" ,
                                                           HotelCode = "H-3" ,
                                                           Guid = Common.GetUniqueId() ,
                                                           Location = "1 Minto Road G, Dhaka City 1000 Bangladesh" ,
                                                            Rating = 7,
                                                           RStatus = EnumRStatus.Active ,
                                                           CreatedBy = 1 ,
                                                           CreatedDate = DateTime.Now
                                                       });
        }
    }
}
