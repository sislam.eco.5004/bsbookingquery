﻿using System.Data;
using System.Reflection;
using Dapper;
using Microsoft.EntityFrameworkCore;


namespace Repository.Dapper
{
    public interface IDapperRepository
    {
        IEnumerable<T> GetList<T>(string query , object obj);
        IEnumerable<T> GetList<T>(string query , object obj , CommandType commandType);       
        Task<IEnumerable<T>> GetListAsync<T>(string query , object obj);
        Task<IEnumerable<T>> GetListAsync<T>(string query , object obj , CommandType commandType);
        T FirstOrDefault<T>(string query , object obj);
        Task<T> FirstOrDefaultAsync<T>(string query , object obj);
        T FirstOrDefault<T>(string query , object obj , CommandType commandType);
        Task<T> FirstOrDefaultAsync<T>(string query , object obj , CommandType commandType);
        IEnumerable<IDictionary<string , object>> GetDynamicColumnData(string query , object obj);
        void CommandQuery(string query , object obj , CommandType commandType);
        void CommandQuery(string query , CommandType commandType);
        Task CommandQueryAsync(string query , object obj , CommandType commandType);
        Task CommandQueryAsync(string query , CommandType commandType);

        IDbTransaction BeginTransaction();
        IDbConnection GetConnection();

        IEnumerable<T> FindByCondition<T>(object expression);
    }
    public class DapperRepository : IDapperRepository
    {
        private readonly IDbConnection _connection;
        public DbEntity Db { get; set; }

        public DapperRepository(IDbConnection connection, DbEntity repositoryContext)
        {
            _connection = connection;
            Db = repositoryContext;
        }
        public IEnumerable<T> GetList<T>(string query , object obj)
        {
            return _connection.Query<T>(query , obj);
        }       
        public IEnumerable<T> GetList<T>(string query , object obj , CommandType commandType)
        {
            return _connection.Query<T>(query , obj , commandType: commandType);
        }
        
        public IEnumerable<T> FindByCondition<T>(object expression)
        {
            var tableName = Db.Model.FindEntityType(typeof(T)).GetTableName();
            var schemeName = Db.Model.FindEntityType(typeof(T)).GetSchema();
            schemeName = string.IsNullOrEmpty(schemeName) ? "dbo" : schemeName;

            Type myType = expression.GetType();
            IList<PropertyInfo> props = new List<PropertyInfo>(myType.GetProperties());
            var qheryExpression = "";
            foreach (PropertyInfo prop in props)
            {
                object propValue = prop.GetValue(expression, null);
                qheryExpression += $" {prop.Name} = {propValue} ";
                // Do something with propValue
            }
            return null;
            //return _connection.QueryFirstOrDefault<T>($"SELECT * FROM {schemeName}.{tableName} WHERE Id = @id", new { id });
        }
        public async Task<IEnumerable<T>> GetListAsync<T>(string query , object obj)
        {
            return await _connection.QueryAsync<T>(query , obj);
        }
        public async Task<IEnumerable<T>> GetListAsync<T>(string query , object obj , CommandType commandType)
        {
            return await _connection.QueryAsync<T>(query , obj , commandType: commandType);
        }
        public T FirstOrDefault<T>(string query , object obj)
        {
            return _connection.QueryFirstOrDefault<T>(query , obj);
        }
        public async Task<T> FirstOrDefaultAsync<T>(string query , object obj)
        {
            return await _connection.QueryFirstOrDefaultAsync<T>(query , obj);
        }
        public T FirstOrDefault<T>(string query , object obj , CommandType commandType)
        {
            return _connection.QueryFirstOrDefault<T>(query , obj , commandType: commandType);
        }
        public async Task<T> FirstOrDefaultAsync<T>(string query , object obj , CommandType commandType)
        {
            return await _connection.QueryFirstOrDefaultAsync<T>(query , obj , commandType: commandType);
        }
        public IEnumerable<IDictionary<string , object>> GetDynamicColumnData(string query , object obj)
        {
            return _connection.Query(query , obj) as IEnumerable<IDictionary<string , object>>;
        }
        public void CommandQuery(string query , object obj , CommandType commandType)
        {
            _connection.Execute(query , obj , commandType: commandType);
        }
        public void CommandQuery(string query , CommandType commandType)
        {
            _connection.Execute(query , commandType: commandType);
        }
        public async Task CommandQueryAsync(string query , object obj , CommandType commandType)
        {
            await _connection.ExecuteAsync(query , obj , commandType: commandType);
        }
        public async Task CommandQueryAsync(string query , CommandType commandType)
        {
            await _connection.ExecuteAsync(query , commandType: commandType);
        }

        public IDbTransaction BeginTransaction()
        {
            _connection.Open();
            return _connection.BeginTransaction();
        }

        public IDbConnection GetConnection()
        {
            return _connection;
        }
    }
}