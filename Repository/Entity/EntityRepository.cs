﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using Dapper;
using Microsoft.EntityFrameworkCore;
using Model.Enum;
using Utilities.Common;

namespace Repository.Repo.Entity
{
    public interface IEntityRepository<T, TId>
    {
        #region dapper common function
        IEnumerable<T> GetAll(int rStatus = 0);
        T GetById(dynamic id);
        #endregion

        IQueryable<T> FindAll();
        IQueryable<T> FindByCondition(Expression<Func<T, bool>> expression);
        void Create(T entity);
        void Create(IEnumerable<T> entities);
        void Update(T entity);
        void Update(IEnumerable<T> entity);
        void Delete(T entity);
        void Delete(List<T> entities);
        void Delete(TId id);
        /// <summary>
        /// soft delete by condition
        /// </summary>
        /// <param name="expression"></param>
        void DeleteByCondition(Expression<Func<T, bool>> expression);
        void HardDeleteByCondition(Expression<Func<T, bool>> expression);
        void HardDelete(T entity);
        void HardDelete(List<T> entities);
        bool Save();
        Task<bool> SaveAsync();
        DbEntity Db { get; set; }
    }

    public class EntityRepository<T, TId> : IEntityRepository<T, TId> where T : class
    {
        private readonly IDbConnection _connection;
        public DbEntity Db { get; set; }

        public EntityRepository(
            IDbConnection connection,
            DbEntity repositoryContext)
        {
            _connection = connection;
            Db = repositoryContext;
        }

        public IEnumerable<T> GetAll(int rStatus = 0)
        {
            var tableName = Db.Model.FindEntityType(typeof(T)).GetTableName();
            var schemeName = Db.Model.FindEntityType(typeof(T)).GetSchema();
            schemeName = string.IsNullOrEmpty(schemeName) ? "dbo" : schemeName;
            return _connection.Query<T>($"SELECT * FROM {schemeName}.{tableName} WHERE (RStatus = @rStatus OR @rStatus = 0 )", new { rStatus });
        }
        public T GetById(dynamic id)
        {
            var tableName = Db.Model.FindEntityType(typeof(T)).GetTableName();
            var schemeName = Db.Model.FindEntityType(typeof(T)).GetSchema();
            schemeName = string.IsNullOrEmpty(schemeName) ? "dbo" : schemeName;
            return _connection.QueryFirstOrDefault<T>($"SELECT * FROM {schemeName}.{tableName} WHERE Id = @id", new { id });
        }

        public virtual IQueryable<T> FindAll()
        {
            return this.Db.Set<T>().AsNoTracking();
        }

        public virtual IQueryable<T> FindByCondition(Expression<Func<T, bool>> expression)
        {
            return this.Db.Set<T>().Where(expression).AsNoTracking();
        }

        public virtual IQueryable<T> FindByCondition(Expression<Func<T, bool>> expression, Func<IQueryable<T>, IQueryable<T>> func)
        {
            IQueryable<T> result = this.Db.Set<T>().AsQueryable();

            IQueryable<T> resultWithEagerLoading = func(result);

            return resultWithEagerLoading.Where(expression).AsNoTracking();
        }

        public virtual void Create(T entity)
        {
            SetNavigationPropertyCreateValue(entity, 1);
            this.Db.Set<T>().Add(entity);
        }
        public virtual void Create(IEnumerable<T> entities)
        {
            entities.ToList().ForEach(entity => SetNavigationPropertyCreateValue(entity, 1));
            this.Db.Set<T>().AddRange(entities);
        }
        public virtual void Update(T entity)
        {
            SetNavigationPropertyCreateValue(entity, 2);
            this.Db.Entry(entity).State = EntityState.Modified;
        }
        public virtual void Update(IEnumerable<T> entity)
        {
            entity.ToList().ForEach(x =>
            {
                SetNavigationPropertyCreateValue(x, 2);
                this.Db.Entry(x).State = EntityState.Modified;
            });
        }
        public virtual void Delete(T entity)
        {
            SetNavigationPropertyCreateValue(entity, 3);
            this.Db.Entry(entity).State = EntityState.Modified;
        }
        public virtual void Delete(TId id)
        {
            var entity = this.Db.Set<T>().Find(id);
            SetNavigationPropertyCreateValue(entity, 3);
            this.Db.Entry(entity).State = EntityState.Modified;
        }
        public virtual void Delete(List<T> entities)
        {
            entities.ToList().ForEach(x =>
            {
                SetNavigationPropertyCreateValue(x, 3);
                this.Db.Entry(x).State = EntityState.Modified;
            });
        }
        public virtual void DeleteByCondition(Expression<Func<T, bool>> expression)
        {
            var entities = this.Db.Set<T>().Where(expression).AsNoTracking();
            entities.ToList().ForEach(x =>
            {
                SetNavigationPropertyCreateValue(x, 3);
                this.Db.Entry(x).State = EntityState.Modified;
            });
        }
        public virtual void HardDelete(T entity)
        {
            this.Db.Set<T>().Remove(entity);
        }
        public virtual void HardDelete(List<T> entities)
        {
            this.Db.Set<T>().RemoveRange(entities);
        }
        public virtual void HardDeleteByCondition(Expression<Func<T, bool>> expression)
        {
            var entities = this.Db.Set<T>().Where(expression).AsNoTracking();
            entities.ToList().ForEach(x =>
            {
                this.Db.Set<T>().RemoveRange(entities);
            });
        }
        public virtual bool Save()
        {
            var changes = Db.SaveChanges();
            return changes > 0;
        }
        public virtual async Task<bool> SaveAsync()
        {
            var changes = await Db.SaveChangesAsync();
            return changes > 0;
        }

        private void SetNavigationPropertyCreateValue(object entity, int operationType)
        {
            if (entity != null)
            {

                if (operationType == 1)
                {
                    //added
                    entity.GetType().GetProperty("CreatedDate")?.SetValue(entity, Common.GetDateLocalDateTime());
                    entity.GetType().GetProperty("CreatedBy")?.SetValue(entity, 1);
                    entity.GetType().GetProperty("RStatus")?.SetValue(entity, (int)EnumRStatus.Active);
                }
                else if (operationType == 2)
                {
                    entity.GetType().GetProperty("UpdatedDate")?.SetValue(entity, Common.GetDateLocalDateTime());
                    entity.GetType().GetProperty("UpdatedBy")?.SetValue(entity, 1);
                }

                else if (operationType == 3)
                {
                    entity.GetType().GetProperty("DeletedDate")?.SetValue(entity, Common.GetDateLocalDateTime());
                    entity.GetType().GetProperty("DeletedBy")?.SetValue(entity, 1);
                    entity.GetType().GetProperty("RStatus")?.SetValue(entity, (int)EnumRStatus.Deleted);
                }

            }
        }
    }
}


