using System.Reflection;
using Microsoft.EntityFrameworkCore;
using Repository.EntityModel;

namespace Repository.Configuration
{
    public static class BaseEntityConfiguration
    {
        static void Configure<TEntity, T>(ModelBuilder modelBuilder)
            where TEntity : BaseEntity<T>
        {
            modelBuilder.Entity<TEntity>(builder =>
            {
                builder.Property(e => e.RStatus).HasColumnOrder(101);
                builder.Property(e => e.CreatedBy).HasColumnOrder(102);
                builder.Property(e => e.CreatedDate).HasColumnOrder(103);
                builder.Property(e => e.UpdatedBy).HasColumnOrder(104);
                builder.Property(e => e.UpdatedDate).HasColumnOrder(105);
                builder.Property(e => e.DeletedBy).HasColumnOrder(106);
                builder.Property(e => e.DeletedDate).HasColumnOrder(107);
            });
        }

        public static ModelBuilder ApplyBaseEntityConfiguration(this ModelBuilder modelBuilder)
        {
            var method = typeof(BaseEntityConfiguration).GetTypeInfo().DeclaredMethods
                .Single(m => m.Name == nameof(Configure));
            foreach (var entityType in modelBuilder.Model.GetEntityTypes())
            {
                if (entityType.ClrType.IsBaseEntity(out var T))
                    method.MakeGenericMethod(entityType.ClrType, T).Invoke(null, new[] { modelBuilder });
            }
            return modelBuilder;
        }

        static bool IsBaseEntity(this Type type, out Type t)
        {
            for (var baseType = type.BaseType; baseType != null; baseType = baseType.BaseType)
            {
                if (baseType.IsGenericType && baseType.GetGenericTypeDefinition() == typeof(BaseEntity<>))
                {
                   t = baseType.GetGenericArguments()[0];
                    return true;
                }
            }
            t = null;
            return false;
        }
    }
}