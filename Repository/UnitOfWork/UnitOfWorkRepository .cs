﻿using Microsoft.EntityFrameworkCore.Storage;
using Repository.Repo.Entity;
using Repository.Repo;
using System.Data;

namespace Repository.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        bool BeginNewTransaction();
        bool RollBackTransaction();
        IEntityRepository<T , TId> GetRepository<T, TId>() where T : class;
        int SaveChanges();
        public IHotelRepository Hotel {
            get;
        }
        public ICustomerRepository Customer {
            get;
        }
        public ICommentRepository Comment {
            get;
        }
        public IReplyRepository Reply {
            get;
        }
        public IStaffRepository Staff {
            get;
        }
    }
    public class UnitOfWork : IUnitOfWork
    {
        public IHotelRepository Hotel {
            get;
            set;
        }
        public ICustomerRepository Customer {
            get;
            private set;
        }
        public ICommentRepository Comment {
            get;
            private set;
        }
        public IReplyRepository Reply {
            get;
            private set;
        }
        public IStaffRepository Staff {
            get;
            private set;
        }
        private readonly DbEntity _dbContext;
        private IDbContextTransaction _transaction;
        private readonly IDbConnection _connection;
        private bool _disposed;

        public UnitOfWork(DbEntity dbContext , IDbConnection connection)
        {
            _dbContext = dbContext;
            _connection = connection;
            Hotel = new HotelRepository(_connection , _dbContext);
            Customer = new CustomerRepository(_connection , _dbContext);
            Comment = new CommentRepository(_connection , _dbContext);
            Reply = new ReplyRepository(_connection , _dbContext);
        }

        public bool BeginNewTransaction()
        {
            try
            {
                _transaction = _dbContext.Database.BeginTransaction();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    _dbContext.Dispose();
                }
            }

            this._disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public IEntityRepository<T , TId> GetRepository<T, TId>() where T : class
        {
            return new EntityRepository<T , TId>(_connection , _dbContext);
        }

        public bool RollBackTransaction()
        {
            try
            {
                _transaction.Rollback();
                _transaction = null;
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public  int SaveChanges()
        {
            var transaction = _transaction != null ? _transaction : _dbContext.Database.BeginTransaction();

            using (transaction)
            {
                try
                {
                    // Context boş ise hata fırlatıyoruz
                    if (_dbContext == null)
                    {
                        throw new ArgumentException("Context is null");
                    }

                    // SaveChanges metodundan dönen int result ı yakalayarak geri dönüyoruz.
                    int result = _dbContext.SaveChanges();

                    // Sorun yok ise kuyruktaki tüm işlemleri commit ederek bitiriyoruz.
                    transaction.Commit();
                    return result;
                }
                catch (Exception ex)
                {
                    // Hata ile karşılaşılır ise işlemler geri alınıyor 
                    transaction.Rollback();
                    throw new Exception("Error on SaveChanges " , ex);
                }
            }
        }
    }
}
