﻿using Model.EntityModel;
using Repository.Dapper;
using Repository.Repo.Entity;
using System.Data;


namespace Repository.Repo
{
    public interface IReplyRepository : IEntityRepository<ReplyModel , int>
    {
    }

    public class ReplyRepository : EntityRepository<ReplyModel , int>, IReplyRepository
    {
        DbEntity _db;
        public ReplyRepository(IDbConnection connection , DbEntity db) : base(connection , db)
        {
            _db = db;
        }
    }
}
