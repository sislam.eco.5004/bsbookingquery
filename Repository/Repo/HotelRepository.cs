﻿using Model.EntityModel;
using Repository.Dapper;
using Repository.Repo.Entity;
using System.Data;


namespace Repository.Repo
{
    public interface IHotelRepository : IEntityRepository<HotelModel , int>
    {
    }

    public class HotelRepository : EntityRepository<HotelModel , int>, IHotelRepository
    {
        DbEntity _db;
        public HotelRepository(IDbConnection connection , DbEntity db) : base(connection , db)
        {
            _db = db;
        }
    }
}
