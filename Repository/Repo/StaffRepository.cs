﻿using Model.EntityModel;
using Repository.Repo.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Repo
{
    public interface IStaffRepository : IEntityRepository<StaffModel , int>
    {
    }

    public class StaffRepository : EntityRepository<StaffModel , int>, IStaffRepository
    {
        DbEntity _db;
        public StaffRepository(IDbConnection connection , DbEntity db) : base(connection , db)
        {
            _db = db;
        }
    }
}
