﻿using Model.EntityModel;
using Repository.Repo.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Repo
{
    public interface ICustomerRepository : IEntityRepository<CustomerModel , int>
    {
    }

    public class CustomerRepository : EntityRepository<CustomerModel , int>, ICustomerRepository
    {
        DbEntity _db;
        public CustomerRepository(IDbConnection connection , DbEntity db) : base(connection , db)
        {
            _db = db;
        }
    }
}
