﻿using Model.EntityModel;
using Repository.Dapper;
using Repository.Repo.Entity;
using System.Data;


namespace Repository.Repo
{
    public interface ICommentRepository : IEntityRepository<CommentModel , int>
    {
    }

    public class CommentRepository : EntityRepository<CommentModel , int>, ICommentRepository
    {
        DbEntity _db;
        public CommentRepository(IDbConnection connection , DbEntity db) : base(connection , db)
        {
            _db = db;
        }
    }
}
