﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Repository.Migrations
{
    public partial class CreateTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Customers",
                columns: table => new
                {
                    RStatus = table.Column<int>(type: "int", nullable: false),
                    CreatedBy = table.Column<int>(type: "int", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedBy = table.Column<int>(type: "int", nullable: true),
                    UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DeletedBy = table.Column<int>(type: "int", nullable: true),
                    DeletedDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Guid = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CustomerCode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Email = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Phone = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Hotels",
                columns: table => new
                {
                    RStatus = table.Column<int>(type: "int", nullable: false),
                    CreatedBy = table.Column<int>(type: "int", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedBy = table.Column<int>(type: "int", nullable: true),
                    UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DeletedBy = table.Column<int>(type: "int", nullable: true),
                    DeletedDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Guid = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    HotelCode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Location = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Rating = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Hotels", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Staffs",
                columns: table => new
                {
                    RStatus = table.Column<int>(type: "int", nullable: false),
                    CreatedBy = table.Column<int>(type: "int", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedBy = table.Column<int>(type: "int", nullable: true),
                    UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DeletedBy = table.Column<int>(type: "int", nullable: true),
                    DeletedDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Guid = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    StafCode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Email = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Phone = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Staffs", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Comments",
                columns: table => new
                {
                    RStatus = table.Column<int>(type: "int", nullable: false),
                    CreatedBy = table.Column<int>(type: "int", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedBy = table.Column<int>(type: "int", nullable: true),
                    UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DeletedBy = table.Column<int>(type: "int", nullable: true),
                    DeletedDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FkHotelId = table.Column<int>(type: "int", nullable: false),
                    FkCustomerId = table.Column<int>(type: "int", nullable: false),
                    Title = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Text = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Guid = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Comments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Comments_Customers_FkCustomerId",
                        column: x => x.FkCustomerId,
                        principalTable: "Customers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Comments_Hotels_FkHotelId",
                        column: x => x.FkHotelId,
                        principalTable: "Hotels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Replies",
                columns: table => new
                {
                    RStatus = table.Column<int>(type: "int", nullable: false),
                    CreatedBy = table.Column<int>(type: "int", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedBy = table.Column<int>(type: "int", nullable: true),
                    UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DeletedBy = table.Column<int>(type: "int", nullable: true),
                    DeletedDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FkCommentId = table.Column<int>(type: "int", nullable: false),
                    FkStafId = table.Column<int>(type: "int", nullable: false),
                    Title = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Text = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Guid = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Replies", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Replies_Comments_FkCommentId",
                        column: x => x.FkCommentId,
                        principalTable: "Comments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Replies_Staffs_FkStafId",
                        column: x => x.FkStafId,
                        principalTable: "Staffs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "Customers",
                columns: new[] { "Id", "CreatedBy", "CreatedDate", "CustomerCode", "DeletedBy", "DeletedDate", "Email", "Guid", "Name", "Phone", "RStatus", "UpdatedBy", "UpdatedDate" },
                values: new object[,]
                {
                    { 1, 1, new DateTime(2023, 1, 10, 1, 27, 17, 171, DateTimeKind.Local).AddTicks(5719), "C-1", null, null, "khalid.eco.5@gmail.com", "84ca62890e4f4c549aeb39a6b416e87fcdbb96ddcd4b47a6840404419eab3340", "Haris", "123654", 1, null, null },
                    { 2, 1, new DateTime(2023, 1, 10, 1, 27, 17, 171, DateTimeKind.Local).AddTicks(5737), "C-2", null, null, "khalid.eco.5@gmail.com", "9c6fcc7d7d7a4c0ea8fe090ff53087871f6ad89465704083be9825a8676446ec", "Ubaydah", "1236545", 1, null, null },
                    { 3, 1, new DateTime(2023, 1, 10, 1, 27, 17, 171, DateTimeKind.Local).AddTicks(5746), "C-3", null, null, "khalid.eco.5@gmail.com", "031dd63442f94284982fa7a3d55384d648c00de710d14b2f901bbcb5b9ad24a2", "Saiful", "1236546", 1, null, null }
                });

            migrationBuilder.InsertData(
                table: "Hotels",
                columns: new[] { "Id", "CreatedBy", "CreatedDate", "DeletedBy", "DeletedDate", "Guid", "HotelCode", "Location", "Name", "RStatus", "Rating", "UpdatedBy", "UpdatedDate" },
                values: new object[,]
                {
                    { 1, 1, new DateTime(2023, 1, 10, 1, 27, 17, 171, DateTimeKind.Local).AddTicks(5950), null, null, "630a7ef7bf7140d8a1b4d4013f3ef9388f79e3ae285249adb529443953990f56", "H-1", "Airport Road, Dhaka Cantonment, Dhaka City 1206 Bangladesh", " Radisson Blu Dhaka Water Garden", 1, 3, null, null },
                    { 2, 1, new DateTime(2023, 1, 10, 1, 27, 17, 171, DateTimeKind.Local).AddTicks(5962), null, null, "969feedb3d934216b450a5766deaa5e2bfc8ef64f7c547dbaff5fc6d49b4ab36", "H-2", "107 Kazi Nazrul Islam Avenue GPO Box 3595, Dhaka City 1215 Bangladesh", "Pan Pacific Sonargaon Dhaka", 1, 5, null, null },
                    { 3, 1, new DateTime(2023, 1, 10, 1, 27, 17, 171, DateTimeKind.Local).AddTicks(5970), null, null, "77b336b6771646b586732dce58abc05060a740329d6949ceaec25228ea700f7f", "H-3", "1 Minto Road G, Dhaka City 1000 Bangladesh", "InterContinental Dhaka, an IHG Hotel", 1, 7, null, null }
                });

            migrationBuilder.InsertData(
                table: "Staffs",
                columns: new[] { "Id", "CreatedBy", "CreatedDate", "DeletedBy", "DeletedDate", "Email", "Guid", "Name", "Phone", "RStatus", "StafCode", "UpdatedBy", "UpdatedDate" },
                values: new object[,]
                {
                    { 1, 1, new DateTime(2023, 1, 10, 1, 27, 17, 171, DateTimeKind.Local).AddTicks(5908), null, null, "1@gmail.com", "397d4b997af346088a19610572c7a7727f68f246ffb54910ae48d7b44ffa6b7b", "Haris", "***256", 1, "S-1", null, null },
                    { 2, 1, new DateTime(2023, 1, 10, 1, 27, 17, 171, DateTimeKind.Local).AddTicks(5917), null, null, "2@gmail.com", "f5a1fcedd0de4934b6a87eeec3e61944b444265bdb2e4a3ab299d2dd9c3ed8bb", "Ubaydah", "***257", 1, "S-2", null, null },
                    { 3, 1, new DateTime(2023, 1, 10, 1, 27, 17, 171, DateTimeKind.Local).AddTicks(5925), null, null, "3@gmail.com", "ede9f2c3b74148b0bc7111261672053d50f669ead6e94f2fb4b1c1e0598580dc", "Saiful", "***258", 1, "S-3", null, null }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Comments_FkCustomerId",
                table: "Comments",
                column: "FkCustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_Comments_FkHotelId",
                table: "Comments",
                column: "FkHotelId");

            migrationBuilder.CreateIndex(
                name: "IX_Replies_FkCommentId",
                table: "Replies",
                column: "FkCommentId");

            migrationBuilder.CreateIndex(
                name: "IX_Replies_FkStafId",
                table: "Replies",
                column: "FkStafId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Replies");

            migrationBuilder.DropTable(
                name: "Comments");

            migrationBuilder.DropTable(
                name: "Staffs");

            migrationBuilder.DropTable(
                name: "Customers");

            migrationBuilder.DropTable(
                name: "Hotels");
        }
    }
}
